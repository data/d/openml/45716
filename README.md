# OpenML dataset: ThermalPowerConsumptionMarsExpress60

https://www.openml.org/d/45716

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The data, in terms of context data and thermal power consumption measurements, capture the status of the spacecraft over four Martian years sampled at six different time resolutions ranging from 1 min to 60 min. Here we present the dataset with 60 min time resolution, while other time resolutions can be obtained following the link in the publication. From a data analysis point-of-view, analysing these data presents great challenges - even for the more sophisticated state-of-the-art artificial intelligence methods. In particular, given the heterogeneity, complexity and magnitude of the data, they can be employed in different scenarios and analysed through the prism of a variety of machine learning tasks, such as multi-target regression, learning from data streams, anomaly detection, clustering etc. While analysing MEX"s telemetry data is critical for aiding very important decisions regarding the spacecraft status, it can be used to extract novel knowledge and monitor the spacecrafts" health, but also to benchmark artificial intelligence methods designed for a variety of tasks.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45716) of an [OpenML dataset](https://www.openml.org/d/45716). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45716/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45716/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45716/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

